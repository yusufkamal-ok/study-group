<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout/main');
});

//route blog
Route::get('/','BlogController@home');
Route::get('/create','BlogController@create');
Route::get('/index','BlogController@index');
Route::get('/show/{id}','BlogController@show');
Route::post('/store','BlogController@store');
Route::get('/hapus/{id}','BlogController@hapus');
Route::get('/edit/{id}','BlogController@edit');
Route::post('/update','BlogController@update');
