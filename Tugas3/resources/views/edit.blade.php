@extends('layout.main')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Edit Post</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="/"> Back</a>
        </div>
    </div>
</div>
 
@foreach($mahasiswa as $m)
<form action="/update" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $m->id }}"> <br/>
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama :</strong>
                <input type="text" name="Nama" class="form-control" value="{{$m->Nama}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jurusan :</strong>
                <input type="text" name="Jurusan" class="form-control" value="{{$m->Jurusan}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kelas:</strong>
                <input type="text" name="Kelas" class="form-control" value="{{$m->Kelas}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Angkatan :</strong>
                <input type="text" name="Angkatan" class="form-control" value="{{$m->Angkatan}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email :</strong>
                <input type="text" name="Email" class="form-control" value="{{$m->Email}}">
            </div>
        </div>
        <div class="form-group">
                <strong>Nohp :</strong>
                <input type="text" name="Nohp" class="form-control" value="{{$m->Nohp}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat :</strong>
                <input type="text" name="Alamat" class="form-control" value="{{$m->Alamat}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endforeach
@endsection